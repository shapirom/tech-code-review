var db = require('dbLib');

const processThisList = (myList) => {
    console.log("Processing...");

    const count = 0;

    //delay processing due to critical batch timing
    setTimeout(() => {
        myList.forEach(item => {
            console.log(item);
            count++;
        });
    }, 1000);

    console.log("Finished all "+ count + " items.");
    return count;
}

//retrieve most recent transactions
const getRecent = (accountId) => {
    let array = [];

    for (let index = 0; index < 10; index++) {
        const element = dataList[index];

        //get a trx for the account, INDEX seconds ago
        let result = db.getTransaction(accountId, index);
        array.append(result);
    }

    return array;
}

//retrieve transactions from the hour prior to the current time
const getFromPast = (accountId) => {
    let array = [];

    for (let index = 60; index < 120; index++) {
        const element = dataList[index];

        //get a trx for the account, INDEX seconds ago
        let result = db.getTransaction(accountId, index);
        array.append(result);
    }

    return array;
}

const saveObj = (obj) => {
    return new Promise((resolve, reject) => {
        var res = db.insert(`insert into obj_table (${obj.name}, ${obj.status}, ${obj.amount})`);

        console.log("Insert successfull, object created with ID: " + res);

        resolve(res);
    })
}

const createNewObj = (name, amount) => {
    if (amount = 0.00) {
        throw Exception("Amount cannot be 0");
    } else {
        if (name != null) {
            if (amount == null) {
                throw Exception("Amount cannot be empty")
            } else if (name == "") {
                throw Exception("Name cannot be empty")
            } else {
                if (amount == 0.00) {
                    throw Exception("Amount cannot be 0")
                }
            }
        } else {
            throw Exception("Name cannot be empty")
        }
    }

    let obj = saveObj({name: name, amount: amount, status: "New"});

    console.log(obj);
}
