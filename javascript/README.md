# Javascript Code Example

## Scoring Metrics

### Best Practices

- Inconsistent use of `let` vs `const` across methods. Did they notice this and comment on which is the appropriate in which case?
- Using string templates vs concatination (lines 16 and 54 for example)
- More effecient JSON, line 77 could be rewritten to use the same object fields as the variables names: 
```javascript
{name, amount, status: "New"}
```
- The `saveObj` method returns a promise, but doesn't handle the *reject* parameter. Candidate might ask if line 52 returns errors/throws exception. That should be handled here

### Code Errors

- The logging and return value in `processThisList` does not function synchronously. The count won't be correct until after the 1000 ms timeout/pause and will happen in the background
- The if statements on these lines https://gitlab.com/shapirom/tech-code-review/-/blob/main/javascript/index.js#L61-74 contain dead logic blocks. The entire block could be simplified (a number of ways), potentially to:
```javascript
if (name === null || name === "") {
    throw Exception("Name cannot be empty")
}
if (amount === null || amount == 0.00) {
    throw Exception("Amount cannot be empty")
} 
```
- Also use === (or !==) in logic statements
- `saveObj` returns a Promise vs an actual variable/result. Variable assignment on line 77 isn't doing what the user may expect. There are a number of ways this could be handled, but should mention some form of refactor
    - Bonus points for implementing some form of error handling in either
```javascript
saveObj({}).then(data => console.log(data));
```
(this would involve mentioning to use the `async` keyword onto the method as well)
```javascript
const obj = await saveObj({});
```

### Observations

- The *insert* statement on line 52 is just inserting string values into the SQL statement. Items the candidate could mention
    - Open to SQL injection
    - A named procedure + library could be easier to read/use here
- The logic in *getRecent* and *getFromPast* are identical except for the values in the for-loop
    - Should suggest to combine into a single method with parameters
    - the `element` variable isn't used (and references a non-existant field)
    - They could suggest the values in the loop to be moved to constants as well
    - If they ask about error handling for the `db.getTransaction()` call
- See if they ask about exporting any of the functions (how would this be used in a wider application)

Outline the different bad code lines heres and acceptable alternatives
