# Tech Code Review


## Name
Tech Screening Walkthrough

## Description
Providing code examples in different languages, either with explicit errors, or just ineffeciences to be detected and fixed.

## Roadmap
Need additional contributions for:
- C#
- GoLang
- Java
- Python?

Should code be multiple files and/or part of a larger application or ok to be more random in nature?

## Scoring 

Rating a candidate across this code review should not be a simple pass/fail. 

| 1 | 2 | 3 | 4 | 5 |
| ------ | ------ |------ | ------ |------ | 
| Hard Pass | Do Not <br>Recommend | Borderline/Fail<br> Depending on <br>other interviews| Pass | Great work |

We would expect most candidates to fall into the 2-4 range. Scoring should be based on the following criteria:
- Understanding of what the code is doing
- Expectations to scale with level. If there are 10 "known" improvements to the code:
    - a Junior SE would be expected to identify 3
    - Sr SE might get 4-6
    - Staff 7+
- Answers aren't always yes/no, they should also be able to offer _opinions_ about why they make that decision. How well is there understanding of the problem
